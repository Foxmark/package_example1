# Foxmark Tools Master Branch

### Description
This is an example of unofficial composer package setup based on cvs branch functionality
where master branch is the production version. You can also use BETA version by changing your app `composer.json` file

_To read more about branches click:_ [here](https://confluence.atlassian.com/bitbucketserver/using-branches-in-bitbucket-server-776639968.html)

### Installation
From root of your project run:

`composer config repositories.foxmark-example1 vcs git@bitbucket.org:Foxmark/package_example1.git`

and 

```composer require foxmark/package-example-1:dev-master```

or

```composer require foxmark/package-example-1:dev-beta```

_Note: See how after switching to BETA version you will be able to use public getProp method_

You can also create `composer.json` file in root of your project and paste:
```
{
     "repositories": {
         "foxmark-example1": {
             "type": "vcs",
             "url": "git@bitbucket.org:Foxmark/package_example1.git"
         }
     },
     "require": {
         "foxmark/package-example-1": "dev-master"
     }
 }
```
save file and run: `composer install`

Your application code could look like this:

```
<?php

require_once 'vendor/autoload.php';

use Foxmark\Tools as Tools;

$obj = new Tools('Hello World!');
if(method_exists($obj,'getProp')) {
    var_dump($obj->getProp());
} else {
   echo 'To use getProp method switch to BETA branch (dev-beta) in your composer.json file.' . PHP_EOL;
}

```